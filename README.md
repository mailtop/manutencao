[![Executar no Google Cloud Run](https://storage.googleapis.com/cloudrun/button.svg)](https://console.cloud.google.com/cloudshell/editor?shellonly=true&cloudshell_image=gcr.io/cloudrun/button&cloudshell_git_repo=https://gitlab.com/mailtop/manutencao.git)


```bash
docker login registry.gitlab.com
docker run -d -p 80:80 registry.gitlab.com/mailtop/manutencao:master
```